package userenckey

import (
	"bytes"
	"crypto/rand"
	"errors"
	"io"

	"github.com/lunixbochs/struc"
	"github.com/miscreant/miscreant.go"
	"golang.org/x/crypto/argon2"
)

// Current algorithm: Argon2 KDF + AES-SIV key encryption.
const algoArgon2AESSIV = 1

const aeadAlgo = "AES-SIV"

// Struct members have stupid names to reduce the size of the resulting gob!
type argon2Params struct {
	Time    uint32 `struc:"uint32,little"`
	Memory  uint32 `struc:"uint32,little"`
	Threads uint8  `struc:"uint8"`
}

// Default Argon2 parameters are tuned for a high-traffic
// authentication service (<1ms per operation).
var defaultArgon2Params = argon2Params{
	Time:    1,
	Memory:  4 * 1024,
	Threads: 4,
}

const (
	keyLen  = 64
	saltLen = 32
)

func argon2KDF(params argon2Params, salt, pw []byte) []byte {
	return argon2.Key(pw, salt, params.Time, params.Memory, params.Threads, keyLen)
}

// An encrypted container stores an opaque blob of binary data along
// with metadata about the encryption itself, to allow for a
// controlled amount of algorithm malleability accounting for future
// updates. The structure is binary-packed (as opposed to using higher
// level serializations such as encoding/gob) because we'd like to be
// able to read it from other languages if necessary.
type container struct { // nolint: maligned
	Algo    uint8 `struc:"uint8"`
	Params  argon2Params
	SaltLen uint8 `struc:"uint8,sizeof=Salt"`
	Salt    []byte
	DataLen uint16 `struc:"uint16,little,sizeof=Data"`
	Data    []byte
}

// Convert to an opaque encoded ("wire") representation.
func (c *container) Marshal() ([]byte, error) {
	var buf bytes.Buffer
	err := struc.Pack(&buf, c)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// Parse a key object from the wire representation.
func unmarshalContainer(b []byte) (c container, err error) {
	err = struc.Unpack(bytes.NewReader(b), &c)
	return
}

func newContainer(data, pw []byte) (container, error) {
	return encryptArgon2AESSIV(data, pw)
}

func (c container) decrypt(pw []byte) ([]byte, error) {
	// Only one supported kdf/algo combination right now.
	if c.Algo == algoArgon2AESSIV {
		return c.decryptArgon2AESSIV(pw)
	}
	return nil, errors.New("unsupported algo")
}

func (c container) decryptArgon2AESSIV(pw []byte) ([]byte, error) {
	// Run the KDF and create the AEAD cipher.
	dk := argon2KDF(c.Params, c.Salt, pw)
	cipher, err := miscreant.NewAEAD(aeadAlgo, dk, 0)
	if err != nil {
		return nil, err
	}

	// Decrypt the data and obtain the DER-encoded private key.
	dec, err := cipher.Open(nil, nil, c.Data, nil)
	return dec, err
}

func encryptArgon2AESSIV(data, pw []byte) (container, error) {
	c := container{
		Algo:   algoArgon2AESSIV,
		Params: defaultArgon2Params,
		Salt:   genRandomSalt(),
	}

	// Run the KDF and create the AEAD cipher.
	dk := argon2KDF(c.Params, c.Salt, pw)
	cipher, err := miscreant.NewAEAD(aeadAlgo, dk, 0)
	if err != nil {
		return container{}, err
	}

	// Encrypt the data (a DER-encoded ECDSA private key).
	c.Data = cipher.Seal(nil, nil, data, nil)
	return c, nil
}

func genRandomSalt() []byte {
	var b [saltLen]byte
	if _, err := io.ReadFull(rand.Reader, b[:]); err != nil {
		panic(err)
	}
	return b[:]
}
