module git.autistici.org/ai3/go-common

go 1.14

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/amoghe/go-crypt v0.0.0-20220222110647-20eada5f5964
	github.com/bbrks/wrap/v2 v2.5.0
	github.com/cenkalti/backoff/v4 v4.1.3
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/duo-labs/webauthn v0.0.0-20220330035159-03696f3d4499
	github.com/emersion/go-textwrapper v0.0.0-20200911093747-65d896831594
	github.com/fxamacker/cbor/v2 v2.4.0
	github.com/go-asn1-ber/asn1-ber v1.5.4
	github.com/go-ldap/ldap/v3 v3.4.4
	github.com/gofrs/flock v0.8.0 // indirect
	github.com/google/go-cmp v0.5.9
	github.com/lunixbochs/struc v0.0.0-20200707160740-784aaebc1d40
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/miscreant/miscreant.go v0.0.0-20200214223636-26d376326b75
	github.com/prometheus/client_golang v1.12.2
	github.com/russross/blackfriday/v2 v2.1.0
	github.com/theckman/go-flock v0.8.1
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.34.0
	go.opentelemetry.io/contrib/propagators/b3 v1.9.0
	go.opentelemetry.io/otel v1.10.0
	go.opentelemetry.io/otel/exporters/zipkin v1.9.0
	go.opentelemetry.io/otel/sdk v1.10.0
	go.opentelemetry.io/otel/trace v1.10.0
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	golang.org/x/sync v0.1.0
)
