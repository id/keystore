// Package compositetypes provides Go types for the composite values
// stored in our LDAP database, so that various authentication
// packages can agree on their serialized representation.
//
// These are normally 1-to-many associations that are wrapped into
// repeated LDAP attributes instead of separate nested objects, for
// simplicity and latency reasons.
//
// Whenever there is an 'id' field, it's a unique (per-user)
// identifier used to recognize a specific entry on modify/delete.
//
// The serialized values can be arbitrary []byte sequences (the LDAP
// schema should specify the right types for the associated
// attributes).
//
package compositetypes

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/duo-labs/webauthn/protocol/webauthncose"
	"github.com/duo-labs/webauthn/webauthn"
	"github.com/fxamacker/cbor/v2"
)

// AppSpecificPassword stores information on an application-specific
// password.
//
// Serialized as colon-separated fields with the format:
//
//     id:service:encrypted_password:comment
//
// Where 'comment' is free-form and can contain colons, no escaping is
// performed.
type AppSpecificPassword struct {
	ID                string `json:"id"`
	Service           string `json:"service"`
	EncryptedPassword string `json:"encrypted_password"`
	Comment           string `json:"comment"`
}

// Marshal returns the serialized format.
func (p *AppSpecificPassword) Marshal() string {
	return strings.Join([]string{
		p.ID,
		p.Service,
		p.EncryptedPassword,
		p.Comment,
	}, ":")
}

// UnmarshalAppSpecificPassword parses a serialized representation of
// an AppSpecificPassword.
func UnmarshalAppSpecificPassword(s string) (*AppSpecificPassword, error) {
	parts := strings.SplitN(s, ":", 4)
	if len(parts) != 4 {
		return nil, errors.New("badly encoded app-specific password")
	}
	return &AppSpecificPassword{
		ID:                parts[0],
		Service:           parts[1],
		EncryptedPassword: parts[2],
		Comment:           parts[3],
	}, nil
}

// EncryptedKey stores a password-encrypted secret key.
//
// Serialized as colon-separated fields with the format:
//
//     id:encrypted_key
//
// The encrypted key is stored as a raw, unencoded byte sequence.
type EncryptedKey struct {
	ID           string `json:"id"`
	EncryptedKey []byte `json:"encrypted_key"`
}

// Marshal returns the serialized format.
func (k *EncryptedKey) Marshal() string {
	var b []byte
	b = append(b, []byte(k.ID)...)
	b = append(b, ':')
	b = append(b, k.EncryptedKey...)
	return string(b)
}

// UnmarshalEncryptedKey parses the serialized representation of an
// EncryptedKey.
func UnmarshalEncryptedKey(s string) (*EncryptedKey, error) {
	idx := strings.IndexByte(s, ':')
	if idx < 0 {
		return nil, errors.New("badly encoded key")
	}
	return &EncryptedKey{
		ID:           s[:idx],
		EncryptedKey: []byte(s[idx+1:]),
	}, nil
}

// U2FRegistration stores information on a single WebAuthN/U2F device
// registration.
//
// The public key is expected to be in raw COSE format. Note that on
// the wire (i.e. when serialized as JSON) both the public key and the
// key handle are base64-encoded.
//
// It is possible to obtain a usable webauthn.Credential object at
// run-time by calling Decode().
type U2FRegistration struct {
	KeyHandle []byte `json:"key_handle"`
	PublicKey []byte `json:"public_key"`
	Comment   string `json:"comment"`
	Legacy    bool   `json:"-"`
}

// Marshal returns the serialized format.
func (r *U2FRegistration) Marshal() string {
	data, err := json.Marshal(r)
	if err != nil {
		panic(err)
	}
	return string(data)
}

const (
	legacySerializedU2FKeySize = 65
	minU2FKeySize              = 64
)

// UnmarshalU2FRegistration parses a U2FRegistration from its serialized format.
func UnmarshalU2FRegistration(s string) (*U2FRegistration, error) {
	// Try JSON first.
	var reg U2FRegistration
	if err := json.NewDecoder(strings.NewReader(s)).Decode(&reg); err == nil {
		return &reg, nil
	}

	// Deserialize legacy format, and perform a conversion of the
	// public key to COSE format.
	if len(s) < legacySerializedU2FKeySize {
		return nil, errors.New("badly encoded u2f registration")
	}
	b := []byte(s)
	return &U2FRegistration{
		PublicKey: u2fToCOSE(b[:legacySerializedU2FKeySize]),
		KeyHandle: b[legacySerializedU2FKeySize:],
		Legacy:    true,
	}, nil
}

// ParseLegacyU2FRegistrationFromStrings parses the legacy U2F format used
// in manual key specifications etc. which consists of a
// base64(url)-encoded key handle, and a hex-encoded public key (in
// legacy U2F format).
func ParseLegacyU2FRegistrationFromStrings(keyHandle, publicKey string) (*U2FRegistration, error) {
	// U2F key handles are base64(url)-encoded (no trailing =s).
	kh, err := base64.RawURLEncoding.DecodeString(keyHandle)
	if err != nil {
		return nil, fmt.Errorf("error decoding key handle: %w", err)
	}

	// U2F public keys are hex-encoded.
	pk, err := hex.DecodeString(publicKey)
	if err != nil {
		return nil, fmt.Errorf("error decoding public key: %w", err)
	}

	// Simple sanity check for non-empty fields.
	if len(kh) == 0 {
		return nil, errors.New("missing key handle")
	}
	if len(pk) < minU2FKeySize {
		return nil, errors.New("public key missing or too short")
	}

	return &U2FRegistration{
		PublicKey: u2fToCOSE(pk),
		KeyHandle: kh,
		Legacy:    true,
	}, nil
}

// ParseU2FRegistrationFromStrings parses the U2F registration format
// used in manual key specifications that is used by Fido2-aware
// programs such as pamu2fcfg >= 1.0.0. Both parameters are
// base64-encoded, public key should be in COSE format.
func ParseU2FRegistrationFromStrings(keyHandle, publicKey string) (*U2FRegistration, error) {
	kh, err := base64.StdEncoding.DecodeString(keyHandle)
	if err != nil {
		return nil, fmt.Errorf("error decoding key handle: %w", err)
	}
	pk, err := base64.StdEncoding.DecodeString(publicKey)
	if err != nil {
		return nil, fmt.Errorf("error decoding public key: %w", err)
	}

	// Simple sanity check for non-empty fields.
	if len(kh) == 0 {
		return nil, errors.New("missing key handle")
	}
	if len(pk) < minU2FKeySize {
		return nil, errors.New("public key missing or too short")
	}

	return &U2FRegistration{
		PublicKey: pk,
		KeyHandle: kh,
	}, nil
}

// Decode returns a u2f.Registration object with the decoded public
// key ready for use in verification.
func (r *U2FRegistration) Decode() (webauthn.Credential, error) {
	return webauthn.Credential{
		ID:        r.KeyHandle,
		PublicKey: r.PublicKey,
	}, nil
}

// Convert a legacy U2F public key to COSE format.
func u2fToCOSE(pk []byte) []byte {
	var key webauthncose.EC2PublicKeyData
	key.KeyType = int64(webauthncose.EllipticKey)
	key.Algorithm = int64(webauthncose.AlgES256)
	key.XCoord = pk[1:33]
	key.YCoord = pk[33:]
	data, _ := cbor.Marshal(&key) // nolint: errcheck
	return data
}

// Faster, but more questionable, implementation of the above:
//
// func u2fToCOSE(pk []byte) []byte {
// 	x := pk[1:33]
// 	y := pk[33:]
// 	out := []byte{
// 		0xa4,
// 		0x01, 0x02,
// 		0x03, 0x26,
// 		0x21, 0x58, 0x20,
// 	}
// 	out = append(out, x...)
// 	out = append(out, []byte{
// 		0x22, 0x58, 0x20,
// 	}...)
// 	out = append(out, y...)
// 	return out
// }
