package dovecot

import (
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"strings"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/ai3/go-common/userenckey"
	"golang.org/x/sync/singleflight"

	"git.autistici.org/id/keystore/backend"
	ldapBE "git.autistici.org/id/keystore/backend/ldap"
	sqlBE "git.autistici.org/id/keystore/backend/sql"
	"git.autistici.org/id/keystore/client"
)

// Config for the dovecot-keystore daemon.
type Config struct {
	Shard    string                    `yaml:"shard"`
	Backend  *backend.Config           `yaml:"backend"`
	Keystore *clientutil.BackendConfig `yaml:"keystore"`

	// Set this to true if the keys obtained from the backend need
	// to be base64-encoded before being sent to Dovecot.
	Base64Encode bool `yaml:"base64_encode_results"`
}

func (c *Config) check() error {
	if c.Keystore == nil {
		return errors.New("missing keystore config")
	}
	if c.Backend == nil {
		return errors.New("missing backend config")
	}
	return nil
}

// The response returned to userdb lookups. It contains the user's
// public key as a global key for the mail_crypt plugin, and it sets
// mail_crypt_save_version to 2. The idea is that you would then set
// mail_crypt_save_version = 0 in the global Dovecot configuration,
// which would then disable encryption for users without encryption
// keys. For details on what this means, see
// https://wiki2.dovecot.org/Plugins/MailCrypt.
type userdbResponse struct {
	PublicKey   string `json:"mail_crypt_global_public_key"`
	SaveVersion int    `json:"mail_crypt_save_version"`
}

func newUserDBResponse(publicKey string) *userdbResponse {
	return &userdbResponse{
		PublicKey:   publicKey,
		SaveVersion: 2,
	}
}

// The response returned to passdb lookups. We return the user's
// private key and the mail_crypt_save_version attribute as userdb
// parameters (hence the 'userdb_' prefix), and set the noauthenticate
// bit to inform Dovecot that this lookup is only meant to provide
// additional data, not authentication.
type passdbResponse struct {
	PrivateKey  string `json:"userdb_mail_crypt_global_private_key"`
	SaveVersion int    `json:"userdb_mail_crypt_save_version"`
	NoAuth      bool   `json:"noauthenticate"`
}

func newPassDBResponse(privateKey string) *passdbResponse {
	return &passdbResponse{
		PrivateKey:  privateKey,
		SaveVersion: 2,
		NoAuth:      true,
	}
}

var passwordSep = "/"

// KeyLookupProxy interfaces Dovecot with the user encryption key database.
type KeyLookupProxy struct {
	config   *Config
	keystore client.Client
	sfgroup  singleflight.Group
	db       backend.Database
}

// NewKeyLookupProxy returns a KeyLookupProxy with the specified configuration.
func NewKeyLookupProxy(config *Config) (*KeyLookupProxy, error) {
	if err := config.check(); err != nil {
		return nil, err
	}

	ksc, err := client.New(config.Keystore)
	if err != nil {
		return nil, err
	}

	var db backend.Database
	switch config.Backend.Type {
	case "ldap":
		db, err = ldapBE.New(&config.Backend.Params)
	case "sql":
		db, err = sqlBE.New(&config.Backend.Params)
	default:
		err = errors.New("unknown backend type")
	}
	if err != nil {
		return nil, err
	}

	return &KeyLookupProxy{
		config:   config,
		keystore: ksc,
		db:       db,
	}, nil
}

const (
	namespace    = "shared"
	namespaceLen = len(namespace)
)

// Lookup a key using the dovecot dict proxy interface.
//
// We can be sent a userdb lookup, or a passdb lookup, and we can tell
// them apart with the key prefix (passdb/ or userdb/).
func (s *KeyLookupProxy) Lookup(ctx context.Context, key string) (interface{}, bool, error) {
	switch {
	case strings.HasPrefix(key, namespace+"/passdb/"):
		kparts := strings.SplitN(key[namespaceLen+8:], passwordSep, 2)
		return s.lookupPassdb(ctx, kparts[0], kparts[1])
	case strings.HasPrefix(key, namespace+"/userdb/"):
		return s.lookupUserdb(ctx, key[namespaceLen+8:])
	default:
		log.Printf("unknown key %s", key)
		return nil, false, nil
	}
}

func (s *KeyLookupProxy) lookupUserdb(ctx context.Context, username string) (interface{}, bool, error) {
	pub, err := s.db.GetPublicKey(ctx, username)
	if err != nil {
		return nil, false, err
	}
	if pub == nil {
		log.Printf("userdb lookup for %s (no keys)", username)
		return nil, false, nil
	}
	log.Printf("userdb lookup for %s", username)
	return newUserDBResponse(s.b64encode(pub)), true, nil
}

type passdbLookupResult struct {
	obj interface{}
	ok  bool
}

func (s *KeyLookupProxy) lookupPassdb(ctx context.Context, username, password string) (interface{}, bool, error) {
	// In order to wrap lookupPassdb() in a singleflight group we wrap its
	// return parameters in a struct, with some additional error handling.
	res, err, _ := s.sfgroup.Do(username, func() (interface{}, error) {
		obj, ok, err := s.lookupPassdbReal(ctx, username, password)
		if err != nil {
			return nil, err
		}
		return &passdbLookupResult{obj: obj, ok: ok}, nil
	})
	if err != nil {
		return nil, false, err
	}
	plr := res.(*passdbLookupResult)
	return plr.obj, plr.ok, nil
}

func (s *KeyLookupProxy) lookupPassdbReal(ctx context.Context, username, password string) (interface{}, bool, error) {
	// The password might be a SSO token, so first of all we try
	// to fetch the unencrypted key from the keystore daemon.
	var keystoreStatus string
	priv, err := s.keystore.Get(ctx, s.config.Shard, username, password)
	switch {
	case err == client.ErrNoKeys:
		keystoreStatus = "no keys available"
	case isErr403(err):
		keystoreStatus = "no SSO token"
	case err != nil:
		// This is an unexpected error.
		log.Printf("keystore lookup for %s failed: %v", username, err)
		keystoreStatus = fmt.Sprintf("unexpected error: %v", err)
	default:
		log.Printf("passdb lookup for %s (from keystore)", username)
		return newPassDBResponse(s.b64encode(priv)), true, nil
	}

	// Otherwise, fetch encrypted keys from the db and attempt to
	// decrypt them.
	encKeys, err := s.db.GetPrivateKeys(ctx, username)
	if err != nil {
		return nil, false, err
	}
	if len(encKeys) == 0 {
		// If there are no keys in the db, the keystore status
		// is not really relevant.
		log.Printf("no encryption keys for %s in database", username)
		return nil, false, nil
	}
	key, err := userenckey.Decrypt(encKeys, []byte(password))
	if err != nil {
		log.Printf("failed passdb lookup for %s (could not decrypt key), keystore status: %s", username, keystoreStatus)
		return nil, false, err
	}
	priv, err = key.PEM()
	if err != nil {
		log.Printf("failed passdb lookup for %s (obtained invalid key: %v), keystore status: %s", username, err, keystoreStatus)
		return nil, false, err
	}
	log.Printf("passdb lookup for %s (from database)", username)
	return newPassDBResponse(s.b64encode(priv)), true, nil
}

func (s *KeyLookupProxy) b64encode(b []byte) string {
	if s.config.Base64Encode {
		return base64.StdEncoding.EncodeToString(b)
	}
	return string(b)
}

func isErr403(err error) bool {
	if rerr, ok := err.(*clientutil.RemoteError); ok && rerr.StatusCode() == 403 {
		return true
	}
	return false
}
