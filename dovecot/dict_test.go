package dovecot

import (
	"context"
	"io/ioutil"
	"log"
	"net/textproto"
	"os"
	"path/filepath"
	"testing"

	"git.autistici.org/ai3/go-common/unix"
)

func TestUnescape(t *testing.T) {
	for _, td := range []struct {
		input, exp string
	}{
		{"boo", "boo"},
		{"bo\001t", "bo\t"},
		{"bo\001t\001l", "bo\t\n"},
		{"bo\001t\0011", "bo\t\001"},
	} {
		out := make([]byte, len(td.input))
		copy(out, td.input)
		out = unescapeInplace(out)
		if string(out) != td.exp {
			t.Errorf("unescape('%s') returned '%s', expected '%s'", td.input, out, td.exp)
		}
	}
}

type testDictDatabase struct {
	data map[string]interface{}
}

func (d *testDictDatabase) Lookup(_ context.Context, key string) (interface{}, bool, error) {
	value, ok := d.data[key]
	return value, ok, nil
}

func newTestDictDatabase() *testDictDatabase {
	return &testDictDatabase{
		data: map[string]interface{}{
			"pass/foo": struct {
				Password string `json:"password"`
			}{
				Password: "bar",
			},
		},
	}
}

func newTestSocketServer(t *testing.T) (string, func()) {
	t.Helper()

	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	socketPath := filepath.Join(dir, "socket")

	lsrv := unix.NewLineServer(
		NewDictProxyServer(newTestDictDatabase()))
	srv, err := unix.NewUNIXSocketServer(socketPath, lsrv)
	if err != nil {
		t.Fatal(err)
	}

	errCh := make(chan error, 1)
	go func() {
		errCh <- srv.Serve()
	}()

	return socketPath, func() {
		srv.Close()
		os.RemoveAll(dir)
		if err := <-errCh; err != nil {
			log.Printf("unix socket server error: %v", err)
		}
	}
}

func makeTestRequest(t *testing.T, socketPath string, version int, lookupKey, expected string) {
	conn, err := textproto.Dial("unix", socketPath)
	if err != nil {
		t.Fatalf("dialing socket: %v", err)
	}
	defer conn.Close()

	if err := conn.PrintfLine("H%d\t0\t1\tuser\tpass", version); err != nil {
		t.Fatalf("error writing HELLO: %v", err)
	}

	if err := conn.PrintfLine("L%s", lookupKey); err != nil {
		t.Fatalf("error writing LOOKUP: %v", err)
	}
	resp, err := conn.ReadLine()
	if err != nil {
		t.Fatalf("error reading HELLO response: %v", err)
	}
	if resp != expected {
		t.Fatalf("unexpected response: got '%s', expected '%s'", resp, expected)
	}
}

func TestLookup(t *testing.T) {
	socketPath, cleanup := newTestSocketServer(t)
	defer cleanup()

	makeTestRequest(t, socketPath, 2, "pass/foo", "O{\"password\":\"bar\"}")
	makeTestRequest(t, socketPath, 3, "pass/foo", "O{\"password\":\"bar\"}")
	makeTestRequest(t, socketPath, 2, "unknown", "N")
	makeTestRequest(t, socketPath, 3, "unknown", "N")
}
