package dovecot

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"

	"git.autistici.org/ai3/go-common/unix"
)

var (
	failResponse    = []byte{'F', '\n'}
	noMatchResponse = []byte{'N', '\n'}
)

const (
	supportedDictProtocolVersionMin = 2
	supportedDictProtocolVersionMax = 3
)

// DictDatabase is an interface to a key/value store by way of the Lookup
// method.
type DictDatabase interface {
	// Lookup a key. The result boolean is the presence flag, to
	// avoid having to check the object for nullness. Errors
	// result in failure being propagated upstream to Dovecot.
	Lookup(context.Context, string) (interface{}, bool, error)
}

// DictProxyServer exposes a Database using the Dovecot dict proxy
// protocol (see https://doc.dovecot.org/developer_manual/design/dict_protocol/).
//
// It implements the unix.LineHandler interface from the
// ai3/go-common/unix package.
type DictProxyServer struct {
	db DictDatabase
}

// NewDictProxyServer creates a new DictProxyServer.
func NewDictProxyServer(db DictDatabase) *DictProxyServer {
	return &DictProxyServer{db: db}
}

// ServeLine handles a single command.
func (p *DictProxyServer) ServeLine(ctx context.Context, lw unix.LineResponseWriter, line []byte) error {
	if len(line) < 1 {
		return errors.New("line too short")
	}

	switch line[0] {
	case 'H':
		return p.handleHello(ctx, lw, line[1:])
	case 'L':
		return p.handleLookup(ctx, lw, line[1:])
	default:
		return lw.WriteLine(failResponse)
	}
}

func (p *DictProxyServer) handleHello(ctx context.Context, lw unix.LineResponseWriter, arg []byte) error {
	fields := splitFields(arg)
	if len(fields) < 1 {
		return errors.New("could not parse HELLO")
	}

	majorVersion, _ := strconv.Atoi(string(fields[0]))
	if majorVersion < supportedDictProtocolVersionMin || majorVersion > supportedDictProtocolVersionMax {
		return fmt.Errorf("unsupported protocol version %d", majorVersion)
	}

	return nil
}

func (p *DictProxyServer) handleLookup(ctx context.Context, lw unix.LineResponseWriter, arg []byte) error {
	// Support protocol versions 2 and 3 by looking for the \t
	// field separator, which should not appear in the key in
	// version 2 anyway.
	fields := splitFields(arg)
	if len(fields) < 1 {
		return errors.New("could not parse LOOKUP")
	}

	obj, ok, err := p.db.Lookup(ctx, string(fields[0]))
	if err != nil {
		log.Printf("error: %v", err)
		return lw.WriteLine(failResponse)
	}
	if !ok {
		return lw.WriteLine(noMatchResponse)
	}

	var buf bytes.Buffer
	buf.Write([]byte{'O'})
	if err := json.NewEncoder(&buf).Encode(obj); err != nil {
		return err
	}
	// json Encode will already add a newline.
	//buf.Write([]byte{'\n'})
	return lw.WriteLine(buf.Bytes())
}

var dovecotEscapeChars = map[byte]byte{
	'0': 0,
	'1': 1,
	't': '\t',
	'r': '\r',
	'l': '\n',
}

var fieldSeparator = []byte{'\t'}

func unescapeInplace(b []byte) []byte {
	var esc bool
	var j int
	for i := 0; i < len(b); i++ {
		c := b[i]
		if esc {
			if escC, ok := dovecotEscapeChars[c]; ok {
				c = escC
			}
			esc = false
		} else if c == '\001' {
			esc = true
			continue
		}
		b[j] = c
		j++
	}
	return b[:j]
}

func splitFields(b []byte) [][]byte {
	fields := bytes.Split(b, fieldSeparator)
	for i := 0; i < len(fields); i++ {
		fields[i] = unescapeInplace(fields[i])
	}
	return fields
}
