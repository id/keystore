package backend

import (
	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
	ct "git.autistici.org/ai3/go-common/ldap/compositetypes"
	"github.com/go-ldap/ldap/v3"
	"gopkg.in/yaml.v3"

	"git.autistici.org/id/keystore/backend"
)

type ldapQuery struct {
	// SearchBase, SearchFilter and Scope define parameters for
	// the LDAP search. The search should return a single object.
	// SearchBase and SearchFilter can contain the string "%s",
	// which will be replaced with the username before performing
	// a query.
	SearchBase   string `yaml:"search_base"`
	SearchFilter string `yaml:"search_filter"`
	ScopeStr     string `yaml:"scope"`
	scope        int

	// Attr is the LDAP attribute holding the encrypted user key.
	PublicKeyAttr  string `yaml:"public_key_attr"`
	PrivateKeyAttr string `yaml:"private_key_attr"`
}

// Valid returns an error if the configuration is invalid.
func (c *ldapQuery) Valid() error {
	if c.SearchBase == "" {
		return errors.New("empty search_base")
	}
	if c.SearchFilter == "" {
		return errors.New("empty search_filter")
	}
	c.scope = ldap.ScopeWholeSubtree
	if c.ScopeStr != "" {
		s, err := ldaputil.ParseScope(c.ScopeStr)
		if err != nil {
			return err
		}
		c.scope = s
	}

	// Since two different daemons use this code, with separate
	// configs, and one of them does not need one of these
	// attributes, it's more practical to set defaults rather than
	// complain if values are unset.
	if c.PublicKeyAttr == "" {
		c.PublicKeyAttr = "storagePublicKey"
	}
	if c.PrivateKeyAttr == "" {
		c.PrivateKeyAttr = "storageEncryptedSecretKey"
	}
	return nil
}

func (c *ldapQuery) searchRequest(username string, attrs ...string) *ldap.SearchRequest {
	u := ldap.EscapeFilter(username)
	base := strings.Replace(c.SearchBase, "%s", u, -1)
	filter := strings.Replace(c.SearchFilter, "%s", u, -1)
	return ldap.NewSearchRequest(
		base,
		c.scope,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		filter,
		attrs,
		nil,
	)
}

// ldapConfig holds the global configuration for the LDAP user backend.
type ldapConfig struct {
	URI        string     `yaml:"uri"`
	BindDN     string     `yaml:"bind_dn"`
	BindPw     string     `yaml:"bind_pw"`
	BindPwFile string     `yaml:"bind_pw_file"`
	Query      *ldapQuery `yaml:"query"`
}

// Valid returns an error if the configuration is invalid.
func (c *ldapConfig) Valid() error {
	if c.URI == "" {
		return errors.New("empty uri")
	}
	if c.BindDN == "" {
		return errors.New("empty bind_dn")
	}
	if (c.BindPwFile == "" && c.BindPw == "") || (c.BindPwFile != "" && c.BindPw != "") {
		return errors.New("only one of bind_pw_file or bind_pw must be set")
	}
	if c.Query == nil {
		return errors.New("missing query configuration")
	}
	return c.Query.Valid()
}

type ldapBackend struct {
	config *ldapConfig
	pool   *ldaputil.ConnectionPool
}

// New returns a new LDAP backend.
func New(params *yaml.Node) (backend.Database, error) {
	var config ldapConfig
	if err := params.Decode(&config); err != nil {
		return nil, err
	}

	// Validate configuration.
	if err := config.Valid(); err != nil {
		return nil, err
	}

	// Read the bind password.
	bindPw := config.BindPw
	if config.BindPwFile != "" {
		pwData, err := ioutil.ReadFile(config.BindPwFile)
		if err != nil {
			return nil, err
		}
		bindPw = strings.TrimSpace(string(pwData))
	}

	// Connect.
	pool, err := ldaputil.NewConnectionPool(config.URI, config.BindDN, bindPw, 5)
	if err != nil {
		return nil, err
	}

	return &ldapBackend{
		config: &config,
		pool:   pool,
	}, nil
}

// The encrypted private keys are a compound object in LDAP (in
// "id:key" format), we can safely ignore the key id here.
func decodePrivateKey(enc string) ([]byte, error) {
	key, err := ct.UnmarshalEncryptedKey(enc)
	if err != nil {
		return nil, err
	}
	return key.EncryptedKey, nil
}

func (b *ldapBackend) GetPrivateKeys(ctx context.Context, username string) ([][]byte, error) {
	result, err := b.pool.Search(ctx, b.config.Query.searchRequest(username, b.config.Query.PrivateKeyAttr))
	if err != nil {
		return nil, err
	}

	var out [][]byte
	for _, ent := range result.Entries {
		for _, val := range ent.GetAttributeValues(b.config.Query.PrivateKeyAttr) {
			if key, err := decodePrivateKey(val); err == nil {
				out = append(out, key)
			}
		}
	}
	return out, nil
}

func (b *ldapBackend) GetPublicKey(ctx context.Context, username string) ([]byte, error) {
	result, err := b.pool.Search(ctx, b.config.Query.searchRequest(username, b.config.Query.PublicKeyAttr))
	if err != nil {
		return nil, err
	}
	if len(result.Entries) == 0 {
		return nil, nil
	}
	if len(result.Entries) > 1 {
		return nil, fmt.Errorf("public key query for %s returned too many results (%d)", username, len(result.Entries))
	}

	s := result.Entries[0].GetAttributeValue(b.config.Query.PublicKeyAttr)
	if s == "" {
		return nil, nil
	}
	return []byte(s), nil
}
