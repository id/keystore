package backend

import (
	"testing"

	"gopkg.in/yaml.v3"
)

var testYAML = `---
type: "test-type"
params:
  param1: 42
  param2: foo
`

type testParams struct {
	Param1 int    `yaml:"param1"`
	Param2 string `yaml:"param2"`
}

func TestPartialConfigDeserialization(t *testing.T) {
	var c Config
	var p testParams

	if err := yaml.Unmarshal([]byte(testYAML), &c); err != nil {
		t.Fatalf("first unmarshal: %v", err)
	}
	if err := c.Params.Decode(&p); err != nil {
		t.Fatalf("second unmarshal: %v", err)
	}
	if p.Param1 != 42 {
		t.Errorf("param1 is %d, expected 42", p.Param1)
	}
	if p.Param2 != "foo" {
		t.Errorf("param2 is '%s', expected 'foo'", p.Param2)
	}
}
