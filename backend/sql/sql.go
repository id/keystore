package sql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	"gopkg.in/yaml.v3"

	"git.autistici.org/id/keystore/backend"
)

// Names for known SQL queries.
const (
	sqlQueryGetPublicKey   = "get_public_key"
	sqlQueryGetPrivateKeys = "get_private_keys"
)

type sqlConfig struct {
	Driver  string            `yaml:"driver"`
	URI     string            `yaml:"db_uri"`
	Queries map[string]string `yaml:"queries"`
}

func (c *sqlConfig) Valid() error {
	if c.Driver == "" {
		return errors.New("empty driver")
	}
	if c.URI == "" {
		return errors.New("empty uri")
	}
	for _, q := range []string{sqlQueryGetPublicKey, sqlQueryGetPrivateKeys} {
		if _, ok := c.Queries[q]; !ok {
			return fmt.Errorf("missing SQL query '%s'", q)
		}
	}
	return nil
}

type sqlBackend struct {
	db       *sql.DB
	pubStmt  *sql.Stmt
	privStmt *sql.Stmt
}

// New returns a new SQL backend.
func New(params *yaml.Node) (backend.Database, error) {
	var config sqlConfig
	if err := params.Decode(&config); err != nil {
		return nil, err
	}
	if err := config.Valid(); err != nil {
		return nil, err
	}

	db, err := sql.Open(config.Driver, config.URI)
	if err != nil {
		return nil, err
	}

	be := sqlBackend{db: db}
	be.pubStmt, err = db.Prepare(config.Queries[sqlQueryGetPublicKey])
	if err != nil {
		db.Close()
		return nil, err
	}
	be.privStmt, err = db.Prepare(config.Queries[sqlQueryGetPrivateKeys])
	if err != nil {
		db.Close()
		return nil, err
	}

	return &be, nil
}

func (b *sqlBackend) GetPrivateKeys(ctx context.Context, username string) ([][]byte, error) {
	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback() // nolint

	rows, err := tx.Stmt(b.privStmt).Query(username)
	if err != nil {
		return nil, err
	}
	var out [][]byte
	for rows.Next() {
		var key []byte
		if err := rows.Scan(&key); err != nil {
			return nil, err
		}
		out = append(out, key)
	}
	return out, nil
}

func (b *sqlBackend) GetPublicKey(ctx context.Context, username string) ([]byte, error) {
	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}
	defer tx.Rollback() // nolint

	row := tx.Stmt(b.privStmt).QueryRow(username)
	var key []byte
	if err := row.Scan(&key); err != nil {
		if err == sql.ErrNoRows {
			// No rows is not an error.
			return nil, nil
		}
		return nil, err
	}
	return key, nil
}
