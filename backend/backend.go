package backend

import (
	"context"

	"gopkg.in/yaml.v3"
)

// Database represents the interface to the underlying backend for
// encrypted user keys.
type Database interface {
	GetPublicKey(context.Context, string) ([]byte, error)
	GetPrivateKeys(context.Context, string) ([][]byte, error)
}

// Config is how users configure a database backend.
type Config struct {
	Type   string    `yaml:"type"`
	Params yaml.Node `yaml:"params"`
}
