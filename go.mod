module git.autistici.org/id/keystore

go 1.15

require (
	git.autistici.org/ai3/go-common v0.0.0-20221125154433-06304016b1da
	git.autistici.org/id/go-sso v0.0.0-20230822064459-ed921a53bb33
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/go-ldap/ldap/v3 v3.4.4
	github.com/go-sql-driver/mysql v1.7.0
	github.com/lib/pq v1.10.7
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/prometheus/client_golang v1.12.2
	golang.org/x/crypto v0.0.0-20220829220503-c86fa9a7ed90
	golang.org/x/sync v0.3.0
	gopkg.in/yaml.v3 v3.0.1
)
