package main

import (
	"flag"
	"log"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"gopkg.in/yaml.v3"

	"git.autistici.org/ai3/go-common/serverutil"

	"git.autistici.org/id/keystore/server"
)

// func init() {
// 	if err := syscall.Mlockall(syscall.MCL_CURRENT | syscall.MCL_FUTURE); err != nil {
// 		panic(err)
// 	}
// }

var (
	addr       = flag.String("addr", ":5006", "address to listen on")
	configFile = flag.String("config", "/etc/keystore/config.yml", "path of config file")
)

// Config wraps the keystore server.Config together with the HTTP
// server config in a single object for YAML deserialization.
type Config struct {
	server.Config `yaml:",inline"`
	ServerConfig  *serverutil.ServerConfig `yaml:"http_server"`
}

func loadConfig() (*Config, error) {
	// Read YAML config.
	f, err := os.Open(*configFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	var config Config
	if err := yaml.NewDecoder(f).Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	config, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	ks, err := server.NewKeyStore(&config.Config)
	if err != nil {
		log.Fatal(err)
	}
	prometheus.MustRegister(server.NewKeystoreCollector(ks))

	srv := server.NewServer(ks)

	if err := serverutil.Serve(srv, config.ServerConfig, *addr); err != nil {
		log.Fatal(err)
	}
}
