package server

import (
	"context"
	"errors"
	"io/ioutil"
	"log"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ai3/go-common/userenckey"
	"git.autistici.org/id/go-sso"

	"git.autistici.org/id/keystore/backend"
	ldapBE "git.autistici.org/id/keystore/backend/ldap"
	sqlBE "git.autistici.org/id/keystore/backend/sql"
)

var (
	errNoKeys       = errors.New("no keys available")
	errBadUser      = errors.New("username does not match authentication token")
	errUnauthorized = errors.New("unauthorized")
	errInvalidTTL   = errors.New("invalid ttl")
)

type userKey struct {
	pkey []byte
}

type userSession struct {
	id       string
	username string
	expiry   time.Time
	index    int
}

// Config for the KeyStore.
type Config struct {
	SSOPublicKeyFile string `yaml:"sso_public_key_file"`
	SSOService       string `yaml:"sso_service"`
	SSODomain        string `yaml:"sso_domain"`

	Backend *backend.Config `yaml:"backend"`
}

func (c *Config) check() error {
	if c.SSOService == "" {
		return errors.New("sso_service is empty")
	}
	if !strings.HasSuffix(c.SSOService, "/") {
		return errors.New("sso_service is invalid (does not end with /)")
	}
	if c.SSODomain == "" {
		return errors.New("sso_domain is empty")
	}
	if c.Backend == nil {
		return errors.New("missing backend config")
	}
	return nil
}

// KeyStore holds decrypted secrets for users in memory for a short
// time (of the order of a SSO session lifespan). User secrets can be
// opened with a password (used to decrypt the key, which is stored
// encrypted in a database), queried, and closed (forgotten).
//
// The database can provide multiple versions of the encrypted key (to
// support multiple decryption passwords), in which case we'll try
// them all sequentially until one of them decrypts successfully with
// the provided password.
//
// In order to query the KeyStore, you need to present a valid SSO
// token for the user whose secrets you would like to obtain.
//
type KeyStore struct {
	db        backend.Database
	service   string
	validator sso.Validator

	mx       sync.Mutex
	userKeys *userKeyMap
}

func newKeyStoreWithBackend(config *Config, db backend.Database) (*KeyStore, error) {
	ssoKey, err := ioutil.ReadFile(config.SSOPublicKeyFile)
	if err != nil {
		return nil, err
	}
	v, err := sso.NewValidator(ssoKey, config.SSODomain)
	if err != nil {
		return nil, err
	}

	s := &KeyStore{
		userKeys:  newUserKeyMap(),
		service:   config.SSOService,
		validator: v,
		db:        db,
	}
	go s.expireLoop()
	return s, nil
}

// NewKeyStore creates a new KeyStore with the given config and returns it.
func NewKeyStore(config *Config) (*KeyStore, error) {
	if err := config.check(); err != nil {
		return nil, err
	}

	var db backend.Database
	var err error
	switch config.Backend.Type {
	case "ldap":
		db, err = ldapBE.New(&config.Backend.Params)
	case "sql":
		db, err = sqlBE.New(&config.Backend.Params)
	default:
		err = errors.New("unknown backend type")
	}
	if err != nil {
		return nil, err
	}
	return newKeyStoreWithBackend(config, db)
}

func (s *KeyStore) expire(t time.Time) {
	s.mx.Lock()
	s.userKeys.expire(t)
	s.mx.Unlock()
}

func (s *KeyStore) expireLoop() {
	for t := range time.NewTicker(600 * time.Second).C {
		s.expire(t)
	}
}

// Open the user's key store with the given password. If successful,
// the unencrypted user key will be stored for at most ttlSeconds, or
// until Close is called with the same session ID.
//
// Note that the key is fetched from the backend and decrypted even if
// we already have it in memory (for instance belonging to a separate
// session), because this acts as an implicit ACL check: does the user
// have access to the key because it can decrypt it with the provided
// credentials?
//
// A Context is needed because this method might issue an RPC.
//
func (s *KeyStore) Open(ctx context.Context, username, password, sessionID string, ttlSeconds int) error {
	if ttlSeconds == 0 {
		return errInvalidTTL
	}

	encKeys, err := s.db.GetPrivateKeys(ctx, username)
	if err != nil {
		return err
	}
	if len(encKeys) == 0 {
		// No keys found. Not an error.
		return errNoKeys
	}

	// Naive and inefficient way of decrypting multiple keys: it
	// will recompute the kdf every time, which is expensive.
	pkey, err := userenckey.Decrypt(encKeys, []byte(password))
	if err != nil {
		return err
	}
	// Obtain the PEM representation of the private key and store
	// that in memory.
	pem, err := pkey.PEM()
	if err != nil {
		return err
	}

	s.mx.Lock()
	s.userKeys.addSessionWithKey(
		nsSessionID(username, sessionID),
		username,
		&userKey{pkey: pem},
		ttlSeconds)
	s.mx.Unlock()
	return nil
}

// Get the unencrypted key for the specified user. The caller needs to
// provide a valid SSO ticket for the user.
func (s *KeyStore) Get(username, ssoTicket string) ([]byte, error) {
	// Validate the SSO ticket.
	tkt, err := s.validator.Validate(ssoTicket, "", s.service, nil)
	if err != nil {
		// Log authentication failures for debugging purposes.
		log.Printf("Validate(%s) error: %v", username, err)
		return nil, errUnauthorized
	}
	if tkt.User != username {
		log.Printf("Validate(%s) user mismatch: sso=%s", username, tkt.User)
		return nil, errBadUser
	}

	s.mx.Lock()
	defer s.mx.Unlock()
	u, ok := s.userKeys.get(username)
	if !ok {
		return nil, errNoKeys
	}
	return u.pkey, nil
}

// Close the user's key store and wipe the associated unencrypted key
// from memory. Returns true if a key was actually discarded.
func (s *KeyStore) Close(username, sessionID string) bool {
	s.mx.Lock()
	defer s.mx.Unlock()
	return s.userKeys.deleteSession(nsSessionID(username, sessionID))
}

// Combine usernames and session IDs so that every user gets its own
// session namespace. This allows for backwards compatibility for
// clients that do not set a session_id (in which case the service
// automatically reverts to the old behavior).
func nsSessionID(username, sessionID string) string {
	return username + "\000" + sessionID
}
