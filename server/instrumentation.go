package server

import "github.com/prometheus/client_golang/prometheus"

var (
	totalKeysInMemory = prometheus.NewDesc(
		"keystored_keys_total",
		"Total number of unlocked keys in-memory.",
		nil, nil,
	)
	totalSessionsInMemory = prometheus.NewDesc(
		"keystored_sessions_total",
		"Total number of active user sessions.",
		nil, nil,
	)
	requestsCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "keystored_requests_total",
		Help: "Counter of requests by method and status.",
	}, []string{"method", "status"})
	decryptedKeysCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "keystored_decrypted_keys_total",
		Help: "Counter of decrypted keys.",
	})
	unlockedKeysServedCounter = prometheus.NewCounter(prometheus.CounterOpts{
		Name: "keystored_unlocked_keys_served_total",
		Help: "Counter of unlocked keys served.",
	})
)

func init() {
	prometheus.MustRegister(requestsCounter)
	prometheus.MustRegister(decryptedKeysCounter)
	prometheus.MustRegister(unlockedKeysServedCounter)
}

type keystoreCollector struct {
	ks *KeyStore
}

// NewKeystoreCollector returns a prometheus.Collector that will
// export metrics for the given KeyStore instance.
func NewKeystoreCollector(ks *KeyStore) prometheus.Collector {
	return &keystoreCollector{ks: ks}
}

func (c *keystoreCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(c, ch)
}

func (c *keystoreCollector) Collect(ch chan<- prometheus.Metric) {
	c.ks.mx.Lock()
	numKeys := c.ks.userKeys.numCachedKeys()
	numSessions := c.ks.userKeys.numSessions()
	c.ks.mx.Unlock()

	ch <- prometheus.MustNewConstMetric(
		totalKeysInMemory,
		prometheus.GaugeValue,
		float64(numKeys),
	)
	ch <- prometheus.MustNewConstMetric(
		totalSessionsInMemory,
		prometheus.GaugeValue,
		float64(numSessions),
	)
}
