package server

import (
	"bytes"
	"context"
	"net/http/httptest"
	"testing"

	"git.autistici.org/ai3/go-common/clientutil"
	"git.autistici.org/id/keystore/client"
)

type serverTestContext struct {
	*testContext
	keystore *KeyStore
	srv      *httptest.Server
}

func newServerTestContext(t testing.TB) (*serverTestContext, func()) {
	ctx, keystore, cleanup := newTestContext(t)
	srv := httptest.NewServer(NewServer(keystore))

	return &serverTestContext{
			testContext: ctx,
			keystore:    keystore,
			srv:         srv,
		}, func() {
			srv.Close()
			cleanup()
		}
}

func (c *serverTestContext) client(t testing.TB) client.Client {
	kc, err := client.New(&clientutil.BackendConfig{URL: c.srv.URL})
	if err != nil {
		t.Fatalf("client.New(): %v", err)
	}
	return kc
}

func TestServer_OpenAndGet(t *testing.T) {
	c, cleanup := newServerTestContext(t)
	defer cleanup()
	kc := c.client(t)

	// Decrypt the private key with the right password.
	err := kc.Open(context.Background(), "", "testuser", string(pw), "session", 60)
	if err != nil {
		t.Fatal("keystore.Open():", err)
	}

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign("testuser", "keystore/", "domain")
	result, err := kc.Get(context.Background(), "", "testuser", ssoTicket)
	if err != nil {
		t.Fatal("keystore.Get():", err)
	}

	expectedPEM, _ := privKey.PEM()
	if !bytes.Equal(result, expectedPEM) {
		t.Fatalf("keystore.Get() returned bad key: got %v, expected %v", result, expectedPEM)
	}

	// Call Close and forget the key.
	if err := kc.Close(context.Background(), "", "testuser", "session"); err != nil {
		t.Fatalf("keystore.Close(): %v", err)
	}
	if _, err := kc.Get(context.Background(), "", "testuser", ssoTicket); err != client.ErrNoKeys {
		t.Fatalf("keystore.Get() after Close() returned err=%v", err)
	}
}

func TestServer_OpenAndGet_NoKeys(t *testing.T) {
	c, cleanup := newServerTestContext(t)
	defer cleanup()
	kc := c.client(t)

	// Check the return value of Open() when the user has no keys.
	username := "no-keys-user"
	err := kc.Open(context.Background(), "", username, string(pw), "session", 60)
	if err != nil {
		t.Fatalf("keystore.Open(%s): unexpected err=%v", username, err)
	}

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign(username, "keystore/", "domain")
	_, err = kc.Get(context.Background(), "", username, ssoTicket)
	if err != client.ErrNoKeys {
		t.Fatalf("keystore.Get(%s): unexpected err=%v", username, err)
	}
}

func TestServer_OpenAndGet_Unauthorized(t *testing.T) {
	c, cleanup := newServerTestContext(t)
	defer cleanup()
	kc := c.client(t)

	// Valid SSO ticket, but for the wrong user.
	ssoTicket := c.sign("wrong-user", "keystore/", "domain")
	_, err := kc.Get(context.Background(), "", "testuser", ssoTicket)
	if !isErr403(err) {
		t.Fatalf("keystore.Get(wrong-user) returned unexpected err=%v", err)
	}

	// Invalid SSO ticket.
	_, err = kc.Get(context.Background(), "", "testuser", "not-a-valid-sso-ticket")
	if !isErr403(err) {
		t.Fatalf("keystore.Get(invalid-sso) returned unexpected err=%v", err)
	}
}

func isErr403(err error) bool {
	if rerr, ok := err.(*clientutil.RemoteError); ok && rerr.StatusCode() == 403 {
		return true
	}
	return false
}
