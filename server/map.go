package server

import (
	"container/heap"
	"time"
)

// Priority queue of userSession objects, kept ordered by expiration
// time so that periodic expire() is fast even with lots of objects.
type sessionPQ []*userSession

func (pq sessionPQ) Len() int {
	return len(pq)
}

func (pq sessionPQ) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq sessionPQ) Less(i, j int) bool {
	return pq[i].expiry.Before(pq[j].expiry)
}

func (pq *sessionPQ) Push(x interface{}) {
	n := len(*pq)
	item := x.(*userSession)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *sessionPQ) Pop() interface{} {
	old := *pq
	n := len(old)
	x := old[n-1]
	old[n-1] = nil
	*pq = old[0 : n-1]
	return x
}

type sessionMap struct {
	sessions map[string]*userSession
	pq       sessionPQ
}

func newSessionMap() *sessionMap {
	return &sessionMap{
		sessions: make(map[string]*userSession),
	}
}

func (m *sessionMap) add(sessionID, username string, ttl time.Duration) {
	sess := &userSession{
		id:       sessionID,
		username: username,
		expiry:   time.Now().Add(ttl),
	}
	m.sessions[sessionID] = sess
	heap.Push(&m.pq, sess)
}

func (m *sessionMap) del(sessionID string) *userSession {
	sess, ok := m.sessions[sessionID]
	if !ok {
		return nil
	}
	delete(m.sessions, sessionID)
	heap.Remove(&m.pq, sess.index)
	sess.index = -1
	return sess
}

// Peek at the oldest entry and pop it if expired.
func (m *sessionMap) expireNext(deadline time.Time) *userSession {
	if len(m.pq) > 0 && m.pq[0].expiry.Before(deadline) {
		sess := heap.Pop(&m.pq).(*userSession)
		delete(m.sessions, sess.id)
		sess.index = -1
		return sess
	}
	return nil
}

// Maintain association between sessions (that may expire) and user
// keys. Enforces the constraint that keys will be removed once they
// have no sessions attached.
type userKeyMap struct {
	sessions     *sessionMap
	userKeys     map[string]*userKey
	userSessions map[string][]string
}

func newUserKeyMap() *userKeyMap {
	return &userKeyMap{
		sessions:     newSessionMap(),
		userKeys:     make(map[string]*userKey),
		userSessions: make(map[string][]string),
	}
}

func (u *userKeyMap) numCachedKeys() int {
	return len(u.userKeys)
}

func (u *userKeyMap) numSessions() int {
	return u.sessions.pq.Len()
}

func (u *userKeyMap) get(username string) (*userKey, bool) {
	k, ok := u.userKeys[username]
	return k, ok
}

func (u *userKeyMap) addSessionWithKey(sessionID, username string, key *userKey, ttlSeconds int) {
	u.sessions.add(sessionID, username, time.Duration(ttlSeconds)*time.Second)
	u.userKeys[username] = key
	u.userSessions[username] = append(u.userSessions[username], sessionID)
}

func (u *userKeyMap) deleteSession(sessionID string) bool {
	if sess := u.sessions.del(sessionID); sess != nil {
		u.cleanupSession(sess)
		return true
	}
	return false
}

func (u *userKeyMap) expire(deadline time.Time) {
	for {
		sess := u.sessions.expireNext(deadline)
		if sess == nil {
			return
		}
		u.cleanupSession(sess)
	}
}

func (u *userKeyMap) cleanupSession(sess *userSession) {
	var ids []string
	for _, id := range u.userSessions[sess.username] {
		if id != sess.id {
			ids = append(ids, id)
		}
	}

	if len(ids) == 0 {
		// No more sessions for this user, delete key.
		k := u.userKeys[sess.username]
		delete(u.userKeys, sess.username)
		wipeBytes(k.pkey)
		delete(u.userSessions, sess.username)
	} else {
		u.userSessions[sess.username] = ids
	}
}

func wipeBytes(b []byte) {
	for i := 0; i < len(b); i++ {
		b[i] = 0
	}
}
