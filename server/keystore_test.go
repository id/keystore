package server

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"git.autistici.org/ai3/go-common/userenckey"
	"golang.org/x/crypto/ed25519"

	"git.autistici.org/id/go-sso"
)

type testContext struct {
	dir        string
	pubkeyPath string
	signer     sso.Signer
}

func newTestContext(t testing.TB) (*testContext, *KeyStore, func()) {
	dir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatal(err)
	}

	ctx := &testContext{
		dir:        dir,
		pubkeyPath: filepath.Join(dir, "public.key"),
	}

	pub, priv, err := ed25519.GenerateKey(nil)
	if err != nil {
		t.Fatal("ed25519.GenerateKey():", err)
	}
	ctx.signer, err = sso.NewSigner(priv)
	if err != nil {
		t.Fatal("sso.NewSigner():", err)
	}

	ioutil.WriteFile(ctx.pubkeyPath, pub, 0644) // nolint

	db := &testDB{
		keys: map[string][][]byte{
			"testuser": [][]byte{encPrivKey},
		},
	}

	keystore, err := newKeyStoreWithBackend(&Config{
		SSOPublicKeyFile: ctx.pubkeyPath,
		SSOService:       "keystore/",
		SSODomain:        "domain",
	}, db)
	if err != nil {
		t.Fatal("newKeyStoreWithBackend():", err)
	}

	return ctx, keystore, func() {
		ctx.Close()
	}
}

func (c *testContext) Close() {
	os.RemoveAll(c.dir)
}

func (c *testContext) sign(user, service, domain string) string {
	tkt, _ := c.signer.Sign(sso.NewTicket(user, service, domain, "", nil, 600*time.Second))
	return tkt
}

type testDB struct {
	keys map[string][][]byte
}

func (t *testDB) GetPrivateKeys(_ context.Context, username string) ([][]byte, error) {
	keys, ok := t.keys[username]
	if !ok {
		return nil, nil
	}
	return keys, nil
}

func (t *testDB) GetPublicKey(_ context.Context, _ string) ([]byte, error) {
	return nil, errors.New("not implemented")
}

var (
	privKey    *userenckey.Key
	pw         = []byte("equally secret password")
	encPrivKey []byte
)

func init() {
	var err error
	_, privKey, _ = userenckey.GenerateKey()
	encPrivKey, err = userenckey.Encrypt(privKey, pw)
	if err != nil {
		panic(err)
	}
}

func TestKeystore_OpenAndGet(t *testing.T) {
	c, keystore, cleanup := newTestContext(t)
	defer cleanup()

	// Decrypt the private key with the right password.
	err := keystore.Open(context.Background(), "testuser", string(pw), "session", 60)
	if err != nil {
		t.Fatal("keystore.Open():", err)
	}

	// Call expire() now to make sure we don't wipe data that is
	// not expired yet.
	keystore.expire(time.Now())

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign("testuser", "keystore/", "domain")
	result, err := keystore.Get("testuser", ssoTicket)
	if err != nil {
		t.Fatal("keystore.Get():", err)
	}

	expectedPEM, _ := privKey.PEM()
	if !bytes.Equal(result, expectedPEM) {
		t.Fatalf("keystore.Get() returned bad key: got %v, expected %v", result, expectedPEM)
	}

	// Verify user namespace isolation
	keystore.Close("otheruser", "session")
	if _, err := keystore.Get("testuser", ssoTicket); err != nil {
		t.Fatalf("keystore.Get() returned error after Close(otheruser): %v", err)
	}

	// Call Close() and forget the key.
	keystore.Close("testuser", "session")
	if _, err := keystore.Get("testuser", ssoTicket); err == nil {
		t.Fatal("keystore.Get() returned no error after Close()")
	}
}

func TestKeystore_OpenAndGet_BackwardsCompatibility(t *testing.T) {
	c, keystore, cleanup := newTestContext(t)
	defer cleanup()

	// Decrypt the private key with the right password. Do not set a session ID.
	err := keystore.Open(context.Background(), "testuser", string(pw), "", 60)
	if err != nil {
		t.Fatal("keystore.Open():", err)
	}

	// Call expire() now to make sure we don't wipe data that is
	// not expired yet.
	keystore.expire(time.Now())

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign("testuser", "keystore/", "domain")
	result, err := keystore.Get("testuser", ssoTicket)
	if err != nil {
		t.Fatal("keystore.Get():", err)
	}

	expectedPEM, _ := privKey.PEM()
	if !bytes.Equal(result, expectedPEM) {
		t.Fatalf("keystore.Get() returned bad key: got %v, expected %v", result, expectedPEM)
	}

	// Verify user namespace isolation
	keystore.Close("otheruser", "")
	if _, err := keystore.Get("testuser", ssoTicket); err != nil {
		t.Fatalf("keystore.Get() returned error after Close(otheruser): %v", err)
	}

	// Call Close() and forget the key.
	keystore.Close("testuser", "")
	if _, err := keystore.Get("testuser", ssoTicket); err == nil {
		t.Fatal("keystore.Get() returned no error after Close()")
	}
}

func TestKeystore_OpenAndGet_MultipleSessions(t *testing.T) {
	c, keystore, cleanup := newTestContext(t)
	defer cleanup()

	// Decrypt the private key with the right password.
	for _, id := range []string{"session1", "session2"} {
		err := keystore.Open(context.Background(), "testuser", string(pw), id, 60)
		if err != nil {
			t.Fatalf("keystore.Open(%s): %v", id, err)
		}
	}

	// Call expire() now to make sure we don't wipe data that is
	// not expired yet.
	keystore.expire(time.Now())

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign("testuser", "keystore/", "domain")
	result, err := keystore.Get("testuser", ssoTicket)
	if err != nil {
		t.Fatal("keystore.Get():", err)
	}

	expectedPEM, _ := privKey.PEM()
	if !bytes.Equal(result, expectedPEM) {
		t.Fatalf("keystore.Get() returned bad key: got %v, expected %v", result, expectedPEM)
	}

	// Call Close() on the first session, key should still be around.
	keystore.Close("testuser", "session1")
	if _, err := keystore.Get("testuser", ssoTicket); err != nil {
		t.Fatalf("keystore.Get() after Close(session1): %v", err)
	}
	// Closing the second session should wipe the key.
	keystore.Close("testuser", "session2")
	if _, err := keystore.Get("testuser", ssoTicket); err == nil {
		t.Fatal("keystore.Get() returned no error after Close(session2)")
	}
}

func TestKeystore_OpenAndGet_NoKeys(t *testing.T) {
	c, keystore, cleanup := newTestContext(t)
	defer cleanup()

	// Check the return value of Open() when the user has no keys.
	username := "no-keys-user"
	err := keystore.Open(context.Background(), username, string(pw), "session", 60)
	if err != errNoKeys {
		t.Fatalf("keystore.Open() returned unexpected err=%v", err)
	}

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign(username, "keystore/", "domain")
	_, err = keystore.Get(username, ssoTicket)
	if err != errNoKeys {
		t.Fatalf("keystore.Get() returned unexpected err=%v", err)
	}
}

func TestKeystore_Get_Unauthorized(t *testing.T) {
	c, keystore, cleanup := newTestContext(t)
	defer cleanup()

	// Valid SSO ticket, but for the wrong user.
	ssoTicket := c.sign("wrong-user", "keystore/", "domain")
	_, err := keystore.Get("testuser", ssoTicket)
	if err != errBadUser {
		t.Fatalf("keystore.Get(wrong-user) returned unexpected err=%v", err)
	}

	// Invalid SSO ticket.
	_, err = keystore.Get("testuser", "not-a-valid-sso-ticket")
	if err != errUnauthorized {
		t.Fatalf("keystore.Get(invalid-sso) returned unexpected err=%v", err)
	}
}

func TestKeystore_Expire(t *testing.T) {
	c, keystore, cleanup := newTestContext(t)
	defer cleanup()

	// Decrypt the private key with the right password.
	err := keystore.Open(context.Background(), "testuser", string(pw), "session", 60)
	if err != nil {
		t.Fatal("keystore.Open():", err)
	}

	keystore.expire(time.Now().Add(3600 * time.Second))

	// Sign a valid SSO ticket and use it to obtain the private
	// key we just stored.
	ssoTicket := c.sign("testuser", "keystore/", "domain")
	_, err = keystore.Get("testuser", ssoTicket)
	if err != errNoKeys {
		t.Fatal("keystore.Get():", err)
	}
}
